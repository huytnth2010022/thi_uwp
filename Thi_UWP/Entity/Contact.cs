﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thi_UWP.Entity
{
    class Contact
    {
        public int id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
    }
}
