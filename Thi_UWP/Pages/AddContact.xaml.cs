﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Thi_UWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddContact : Page
    {
        public AddContact()
        {
            this.InitializeComponent();
            Loaded += AddContact_Loaded;
        }

        private void AddContact_Loaded(object sender, RoutedEventArgs e)
        {
            SQLiteConnection cnn = new SQLiteConnection("contact.db");

            string sql = @"CREATE TABLE IF NOT EXISTS
                          Contact (Id      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                    Name    VARCHAR( 140 ),
                                    Phone    VARCHAR( 100 ) NOT NULL UNIQUE
                                    );";
            using (var statement = cnn.Prepare(sql))
            {
                statement.Step();
            }
        }

        private void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {

            var MenuItem = sender as MenuFlyoutItem;
            switch (MenuItem.Tag)
            {
                case "add":
                    this.Frame.Navigate(typeof(Pages.AddContact));
                    break;
                case "list":
                    this.Frame.Navigate(typeof(Pages.ListContact));
                    break;
            }
        }

   

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CO contact = new CO()
            {
                name = name.Text,
                phone = phone.Text,
            };

            
                SQLiteConnection cnn = new SQLiteConnection("contact.db");
                ISQLiteStatement stt = cnn.Prepare("insert into Contact (Name,Phone) values (?,?)");
                stt.Bind(1, contact.name);
                stt.Bind(2, contact.phone);
                stt.Step();
            
          
        }
    }
}
