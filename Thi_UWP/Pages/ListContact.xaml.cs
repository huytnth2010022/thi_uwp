﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Thi_UWP.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListContact : Page
    {
        public ListContact()
        {
            this.InitializeComponent();
            Loaded += ListContact_Loaded;
        }

        private void ListContact_Loaded(object sender, RoutedEventArgs e)
        {
            List<CO> contacts = new List<CO>();
            SQLiteConnection cnn = new SQLiteConnection("contact.db");
            using (var stt = cnn.Prepare("select * from Contact"))
            {
                while (stt.Step() == SQLiteResult.ROW)
                {
                    CO contact = new CO()
                    {
                        name = (string)stt["Name"],
                        phone = (string)stt["Phone"],
                    };
                    contacts.Add(contact);
                }
            }
            ListContactView.ItemsSource = contacts;
        }

        private void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {

            var MenuItem = sender as MenuFlyoutItem;
            switch (MenuItem.Tag)
            {
                case "add":
                    this.Frame.Navigate(typeof(Pages.AddContact));
                    break;
                case "list":
                    this.Frame.Navigate(typeof(Pages.ListContact));
                    break;
            }
        }

    }
}
